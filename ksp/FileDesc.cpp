#include "ksp/FileDesc.h"

#ifdef _WIN32 

int FileDesc::Close()
{
    return CloseHandle((HANDLE)fd);
}

int FileDesc::Write(const void* __buf, size_t __n)
{
    unsigned long res;
    bool flag = WriteFile((HANDLE)fd, __buf, __n, &res, 0);
    return res;
};

int FileDesc::Read(void* __buf, size_t __nbytes)
{
    unsigned long res;
    bool flag = ReadFile((HANDLE)fd, __buf, __nbytes, &res, 0);
    return res;
};



#endif

#ifdef __linux__


int FileDesc::Write(const void *__buf, size_t __n)
{
    return write(fd, __buf, __n);
};

int FileDesc::Read(void *__buf, size_t __nbytes)
{
    return read(fd, __buf, __nbytes);
};

int FileDesc::Close()
{
    return close(fd);
};

#endif // Win32
