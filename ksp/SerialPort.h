#pragma once

#include "ksp/pch.h"
#include "ksp/FileDesc.h"

namespace Ksp
{

    struct SerialPort : public FileDesc
    {
        struct Desc
        {
            std::string path = "/dev/ttyS0";
        };

        struct SerialState
        {
            int baudRate;
            unsigned char byteSize;
            unsigned char stopBits;

            /* 0-4=None,Odd,Even,Mark,Space    */
            unsigned char parityBits;
        };

        bool Create(const Desc &desc);
        bool GetState(SerialState &target);
        bool SetState(const SerialState &source);
    };
}
