#include "ksp/pch.h"
#include "ksp/FileDesc.h"
#include "ksp/SerialPort.h"

using namespace Ksp;

int main()
{
    SerialPort::Desc desc;
#ifdef __linux__
    desc.path = "/dev/ttyS0";

#endif
#ifdef _WIN32
    desc.path = "COM4";
#endif
    SerialPort serialport;
    bool gs = serialport.Create(desc);

    if (!gs)
    {
        LOGI("cannot create serial port");
        return 0;
    }
    // 打开串口设备文件
    SerialPort::SerialState state;
    state.baudRate = 9600;

    char name[20] = "hello world";
    int writeLen = serialport.Write(name, sizeof(name));
    int readLen = serialport.Read(name, sizeof(name));
    LOGI("write len %d read len %d", writeLen, readLen);
    // 关闭串口
    serialport.Close();

    return 0;
}
