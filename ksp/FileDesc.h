#pragma once

#include "ksp/pch.h"

struct FileDesc
{
    int fd;

    int Write(const void *__buf, size_t __n);

    int Read(void *__buf, size_t __nbytes);

    int Close();
    
    template<typename T>
    inline int Write(const T& t)
    {
        return Write(&t, sizeof(T));
    }
    template<typename T>
    inline int Read(T& t)
    {
        return Read(&t, sizeof(T));
    }

    template<typename... Args>
    inline int IOCtl(unsigned long int __request, Args&&... args)
    {
        return ioctl(fd, __request, std::forward<Args>(args)...);
    }
};

